
DOCKER_ANSIBLE_IMAGE=kube_ansible
DOCKER_LATEX_IMAGE=kube_latex
ANSIBLE_CONFIG_FILE := group_vars/all/kube_config.yml
VIRTUAL_ENV_DIR = .venv
EDITOR ?= vim
PDFLATEX=pdflatex
LATEXFLAGS=-file-line-error -halt-on-error -interaction errorstopmode --enable-write18

all: pdf

CONTAINERIZED_ANSIBLE := ${VIRTUAL_ENV_DIR} \
			${CONFIG_FILE} \
			config \
			kube \
			shell \
                        virtualenv

CONTAINERIZED_LATEX := pdf

SRC_LATEX=$(wildcard slides/*.tex)

activate_venv := [ "${VIRTUAL_ENV}" = "" ] && . ./${VIRTUAL_ENV_DIR}/bin/activate

define run_image
    docker run -ti --rm \
    -v $$(pwd):/home/build/projects \
    -w /home/build/projects \
    -u $$(id -u)
endef

define find_login
    python -c "import yaml; \
    print(yaml.load(open('$${TEMP_FILE}.yml')).get('onelogin_username', ''))"
endef

.PHONY:check
check:
	aspell --mode=tex --lang=en_US --encoding=UTF-8 -c ansible.tex

.PHONY:clean
clean:
	rm -rf *.aux *.nav *.pdf *.vrb *.log *.out *.snm *.toc

docker-ansible-image:
	docker build dockerfiles/ansible \
	 -t ${DOCKER_ANSIBLE_IMAGE} --build-arg USER=$$(id -u)

docker-latex-image:
	docker build dockerfiles/latex \
	-t ${DOCKER_LATEX_IMAGE} --build-arg USER=$$(id -u)

${ANSIBLE_CONFIG_FILE}:
	@if [ ! -f $@ ]; then (\
	echo "onelogin_username: '$$(git config user.email | tr -d '[:space:]')'";\
	echo "# tenant_name: 'supervisor-paris@scality.com'";\
	echo "region: 'Europe'";\
	echo "# region: 'NorthAmerica'";\
	echo "key_name: 'Scality'";\
	) > $@; fi

mysql:
	helm install --name grafana-mysql incubator/mysqlha --set mysqlha.mysqlUser=grafana --set mysqlha.mysqlDatabase=grafana

ifneq ("${IN_DOCKER}", "1")

.PHONY:
${CONTAINERIZED_ANSIBLE}: docker-ansible-image
	${run_image} -e EDITOR=${EDITOR} ${DOCKER_ANSIBLE_IMAGE} ${MAKE} IN_DOCKER=1 $@

${CONTAINERIZED_LATEX}: docker-latex-image
	${run_image} ${DOCKER_LATEX_IMAGE} ${MAKE} IN_DOCKER=1 $@

else

shell:
	/bin/bash

${VIRTUAL_ENV_DIR}:
	@if [ "$${VIRTUAL_ENV}" = "" ]; then \
	   PYTHON_EXE=$$(which python2 || which python); \
	   VIRTUALENV_EXE=$$(which virtualenv2 || which virtualenv); \
	   $${VIRTUALENV_EXE} $@ -p $${PYTHON_EXE}; \
	else \
	   touch $@; \
	fi;
	${activate_venv}; pip install -r requirements.txt


config: ${VIRTUAL_ENV_DIR} ${ANSIBLE_CONFIG_FILE}
	@TEMP_FILE=`mktemp /tmp/kube_session.XXXXXX`; \
	cp ${ANSIBLE_CONFIG_FILE} $${TEMP_FILE}.yml; \
	$(activate_venv); \
	if [ "`${find_login}`" = "" ]; then \
	   ${EDITOR} $${TEMP_FILE}.yml; \
	fi; \
	mv $${TEMP_FILE}.yml ${ANSIBLE_CONFIG_FILE}


kube: ${VIRTUAL_ENV_DIR} ${ANSIBLE_CONFIG_FILE}
	${activate_venv}; ansible-playbook kube.yml


pdf: ${SRC_LATEX}
	cd $(<D); $(PDFLATEX) $(LATEXFLAGS) $(<F)

endif

# vim: noexpandtab shiftwidth=8 softtabstop=0
