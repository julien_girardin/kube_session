#!/usr/bin/python

import urllib

from ansible.module_utils.basic import *

try:
    import requests
    HAS_REQUESTS = True
except ImportError:
    HAS_REQUESTS = False


DOCUMENTATION = '''
---
module: onelogin_token
short_description: Generate a scality.cloud token from onelogin auth
description:
  - Generate a scality.cloud token from onelogin authentication
options:
  username:
    description:
      - Onelogin username
    required: true
  password:
    description:
      - Onelogin password
    required: true
  tenant:
    description:
      - scality.cloud tenant name
    required: true
requirements:
  - "python >= 2.6"
  - "requests"
'''


class OneLoginTokenModule(AnsibleModule):
    """Ansible module to generate scality.cloud tokens."""

    onelogin_endpoint = 'https://api.us.onelogin.com'
    pf9_endpoint = 'https://scality.cloud'

    client_id = 'fb552c1b6c754307c509260f73e488b4ac8ce682a0ba81762d97e9c1514a5d99'
    client_secret = '276b74c97b475b1ca2c35b2c7d8bee3bcda129c22b86c61f5938cad8ae2bb7da'
    app_id = '597062'
    subdomain = 'scality'
    session = None

    def __init__(self):
        AnsibleModule.__init__(
            self,
            argument_spec={
                'username': {'required': True,
                             'type': 'str'},
                'password': {'required': True,
                             'no_log': True,
                             'type': 'str'},
                'tenant': {'required': True,
                           'type': 'str'},
            }
        )
        self.username = self.params.get('username')
        self.password = self.params.get('password')
        self.tenant = self.params.get('tenant')

    def execute(self):
        """Run the ansible module."""
        self.session = requests.Session()
        access_token = self.get_oauth_token()
        saml_assert = self.get_saml_assert(access_token)
        self.get_os_token(saml_assert)
        os_token = self.get_unscoped_token()
        tenant_id = self.get_tenant(os_token)
        return {
            'changed': True,
            'token': self.get_scoped_token(os_token, tenant_id),
        }

    def get_oauth_token(self):
        """Retrieve an oauth2 token from onelogin."""
        res = self.session.post(
            self.onelogin_endpoint + '/auth/oauth2/token',
            headers={
                'Authorization': 'client_id:%s,client_secret:%s' % (
                    self.client_id,
                    self.client_secret
                ),
            },
            json={'grant_type': 'client_credentials'},
        )
        return res.json()['data'][0]['access_token']

    def get_saml_assert(self, access_token):
        res = self.session.post(
            self.onelogin_endpoint + '/api/1/saml_assertion',
            headers={'Authorization': 'bearer:%s' % access_token},
            json={
                'username_or_email': self.username,
                'password': self.password,
                'app_id': self.app_id,
                'subdomain': self.subdomain
            },
        ).json()
        if res['status']['error']:
            if res['status']['code'] == 400:
                raise OneLoginTokenError('Wrong login')
            else:
                raise OneLoginTokenError(res['status']['message'])
        return res['data']

    def get_os_token(self, saml_assert):
        self.session.post(
            self.pf9_endpoint + '/Shibboleth.sso/SAML2/POST',
            headers={'Content-Type':'application/x-www-form-urlencoded'},
            data=urllib.urlencode({'SAMLResponse': saml_assert}),
        )

    def get_unscoped_token(self):
        """Get unscoped token from scality.cloud."""
        res = self.session.get(
            self.pf9_endpoint + '/keystone_admin/v3/OS-FEDERATION/'
            'identity_providers/IDP1/protocols/saml2/auth'
        )
        return res.headers['X-Subject-Token']

    def get_tenant(self, os_token):
        """Get tenant id associated with tenant_name."""
        res = self.session.get(
            self.pf9_endpoint + '/keystone/v3/OS-FEDERATION/projects',
            headers={'X-Auth-Token': os_token},
        )

        for project in res.json()['projects']:
            if self.tenant == project['name']:
                return project['id']
        raise OneLoginTokenError('Tenant \'%s\' not found' % self.tenant)

    def get_scoped_token(self, os_token, tenant_id):
        """Get scoped token from scality.cloud."""
        res = self.session.post(
            self.pf9_endpoint + '/keystone/v3/auth/tokens?nocatalog',
            json={
                "auth": {
                    "identity": {
                        "methods": ["saml2"],
                        "saml2": {"id": os_token},
                    },
                    "scope": {
                        "project": {"id": tenant_id},
                    }
                }
            }
        )
        return res.headers['X-Subject-Token']


class OneLoginTokenError(Exception):
    """Error during onelogin token generation."""
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)
        try:
            self.msg = args[0]
        except IndexError:
            self.msg = None

def main():
    """Generate a token and return success or failure json to stdout."""
    module = OneLoginTokenModule()

    if not HAS_REQUESTS:
        module.fail_json(msg='requests is required for this module')

    try:
        result = module.execute()
    except OneLoginTokenError as ex:
        module.fail_json(msg=ex.msg)
    else:
        module.exit_json(**result)


if __name__ == '__main__':
    main()
